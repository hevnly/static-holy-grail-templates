//
//
// HEADER REGISTRATION FORM
//
//

var wsHeader = $('.ws-header'),
	wsNavContainer = $('.ws-header-navrow'),
	wsJoinToggleBtn = $('.ws-sitenav-jointoggle'),
	wsGlobRegisterContainer = wsHeader.find('.ws-globsignup'),
	wsGlobRegisterInput = wsGlobRegisterContainer.find('.ws-globsignup-input'),
	wsGlobRegisterButton = wsGlobRegisterContainer.find('.ws-globsignup-button');

wsJoinToggleBtn.on('click', function(event) {

	// show the registration input
	wsHeader.toggleClass('-show-registration');

	// give input focus
	wsGlobRegisterInput.focus();

	// prevent browser from going to new page
	return false;
});



//
//
// INIT CANCEL ZOOM
//
//

$('input, select, textarea').cancelZoom();



//
//
// THE RUG REGISTRATION FORM
//
//

// only call the function if we're on the post page
if ( $('.ws-post-register').length > 0 ) {

	var registerForm = $('.ws-post-register'),
		siteFooter = $('.ws-footer');
	
	$(window).scroll(function() {
		
		if($(window).scrollTop() + $(window).height() == $(document).height()) {
	    	siteFooter.removeClass('-hide');
	    	registerForm.addClass('-flow');
	    }

	});
}



//
//
// WHACK A MOLE REGISTER FORM
//
//

var walEl = $('.ws-whackamole'),
	walInput = walEl.find('input'),
	walBtnClose = walEl.find('.ws-whackamole-close');

$(window).on('scroll', function() {

	if( $(window).scrollTop() >= 2000) {

		walEl.addClass('-show');
		walInput.focus();
	}
});

// hide whackamole when user clicks dismiss
walBtnClose.on('click', function() {
	walEl.removeClass('-show').addClass('-dismissed');
});



//
//
// TAGLINE ROTATOR
//
//

// KOSTAS: sorry dude, I know how horrible this code is.
// Basically we wanna add `-flip-180` + `180` every second and a half, and at 
// some point between the class change, update the text to the next option.
// I'm sure you can figure out a better way to do this!

var flipEl = $('.ws-tagline-container'),
	flipContainer = $('.ws-tagline-flipper'),
	flipFront = flipContainer.find('.ws-tagline-flipper-front'),
	flipBack = flipContainer.find('.ws-tagline-flipper-back');

function slider() {
	
	setTimeout(function() {
		flipContainer.removeClass('-flip-1080').addClass('-flip-180');
		
		setTimeout(function() {
			flipFront.text('authenticity');
		}, 330);

	}, 1000);

	setTimeout(function() {
		flipContainer.removeClass('-flip-180').addClass('-flip-360');

		setTimeout(function() {
			flipBack.text('innovation');
		}, 330);
		
	}, 2500);

	setTimeout(function() {
		flipContainer.removeClass('-flip-360').addClass('-flip-540');

		setTimeout(function() {
			flipFront.text('creativity');
		}, 330);	

	}, 4000);

	setTimeout(function() {
		flipContainer.removeClass('-flip-540').addClass('-flip-720');

		setTimeout(function() {
			flipBack.text('art');
		}, 330);	

	}, 5500);

	setTimeout(function() {
		flipContainer.removeClass('-flip-720').addClass('-flip-900');

		setTimeout(function() {
			flipFront.text('style');
		}, 330);	

	}, 7000);

	setTimeout(function() {
		flipContainer.removeClass('-flip-900').addClass('-flip-1080');

	}, 8500);

	setTimeout(function() {
		flipEl.addClass('-finish');

	}, 1000);	
}

slider();