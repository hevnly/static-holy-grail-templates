# To get the site up & running

Pull down this repo, then start a local server in your local directory, for example `$ php -S localhost:1234`.

In a separate directory, pull down Asset Bazaar. You'll need to compile the LESS (instructions on how to do that are in the README.md on that repo). Then in your Asset Bazaar local copy, start up another server, for example: `$php -S localhost:9876`

Then in this repo, point `index.html`'s css to `magazine.css` in Asset Bazaar.

Voila!

# Dynamic Content for JS to insert into document

### The Post Page

* `.ws-post-header`: add the hero image for the post as a `background-image` via a style attribute. _Important:_ must be `background-image` and not the shorthand property `background`.
* `.ws-post-title`: add the post title
* `.ws-post-subtitle`: add the posts subtitle
* ~~`.ws-post-metadata-edition`: the edition name~~
  * ~~and update the href value to the archive, filtered results for just that edition~~
* <span style="background: #A2E4A2;">`.ws-article-metadata-edition-name`: add the edition name</span>
* <span style="background: #A2E4A2;">`.ws-article-metadata-edition-name`: add `color:` via a `style` attribute. The value should be the edition color.</span>
* `.ws-post-metadata-category-name`: the post category
* <span style="background: #A2E4A2;">`.ws-post-metadata-readtime-time`: number of minutes it takes to read</span>
* Use the title attributes of image tags to create image captions:
	* Take the title attribute value, and then
	* Insert a `<span>` tag inside the containing `<p>` tag and insert the title value into the span tag

### The Archive Page

##### For all article cards:

* <span style="background: #A2E4A2;">`.ws-article`: add `border-bottom-color:`. The value is the edition color</span>
* ~~`.ws-article`~~ `.ws-article-image`: add the articles background image via a style `attr`
* `.ws-article-metadata-edition-name`: add the articles edition name
* <span style="background: #A2E4A2;">`.ws-article-metadata-edition-name`: add `color:` via a `style` attribute. The value is the edition color</span>
* <span style="background: #A2E4A2;">`.ws-article-metadata-edition-name`: add the edition URL to the `href` attribute</span>
* <span style="background: #A2E4A2;">`.ws-article-subtitle`: add the articles sub-title</span>
* `.ws-article-metadata-category-name`: add the articles category name
* `.ws-article-metadata-readtime-time`: add the articles readtime value
* ~~`.ws-editioncluster`: add the edition theme colour to a `background-color` via a style `attr` on this element.~~
* <span style="background: #A2E4A2;">`.ws-article > a`: add the posts `url` to the `href` attributes</span>
* <span style="background: #A2E4A2;">`.ws-article-blockquote-text`: Add the articles blockquote (this will need to be a new meta in the JSON).</span>


##### <span style="background: #A2E4A2;">How to alter the article cards styling</span>

<span style="background: #A2E4A2;">In the previous holy grail site, the article cards didn't share the same mark up. They do now, and we just need to add `-flag` classes to the article containers as styling hooks:</span>

* <span style="background: #A2E4A2;">Lead Edition Articles: Add `.-leadarticle` to the container of the article card</span>
* <span style="background: #A2E4A2;">Quotation Articles: Add `.-quotation` to the container of the article card</span>

_note:_ <span style="background: #A2E4A2;">A slightly tricky rule. When the archive page's intro message has been dismissed, we need to add `.-first` to only the first article that appears on the page. This is because of the way that the global header has been positioned (not in the flow of the document).</span>


# The Post

Every attempt has been made to keep the post mark up as close to what a markdown compiler would produce, however, a few cases haven't been possible.

Markdown compilers automatically wrap inline elements in a block level element. Which means the following markdown:

```
![Alt text](/path/to/img.jpg "Optional title")
```

Will actually compile to:

```
<p>
	<img src="/path/to/img.jpg" alt="Alt text" title="Optional Title">
</p>
```

What we want it to compile to is:

```
<!-- add `.ws-post-thepost-image` to the `<p>` tag -->
<p class="ws-post-thepost-image">
	<img src="/path/to/img.jpg" alt="Alt text" title="Optional Title">
	<!-- create a new `<span>` tag and insert the value of `img[title]` -->
	<span>Optional Title</span>
</p>
```

*note:* the mark up for a post can be seen in `posts.html` line 15-53.


### <span style="background: #A2E4A2;">Modules State Behaviour</span>

##### <span style="background: #A2E4A2;">Hide the magazine intro</span>

<span style="background: #A2E4A2;">When a user clicks `.ws-mag-intro-dismiss`: remove `.-expanded` & `.-inflow` from `.ws-header`</span>

##### <span style="background: #A2E4A2;">Show the sign up form from the global header</span>

<span style="background: #A2E4A2;">When a user clicks `.ws-sitenav-jointoggle`, add `.-show-registration` to `.ws-header`. Then when a user clicks `.ws-globsignup-button` or presses enter, we need to submit the form. A loading state and error messages are on there way for this form.</span>

##### <span style="background: #A2E4A2;">Modeless Register View (whack-a-mole)</span>

<span style="background: #A2E4A2;">When a user scrolls down past _the first big image_ on a page, the magazine should show the whack a mole register form. To show the form, add `-show` to `.ws-whackamole`.</span>

##### <span style="background: #A2E4A2;">Filter by Post Category</span>

<span style="background: #A2E4A2;">When a user clicks on any of the secondary navigation links (`a.ws-mag-catnav-button`), the archive page should then show users only articles from the category selected. To  give users some orientation, we also need to add `.-accent` to the category nav button.</span>

##### <span style="background: #A2E4A2;">Post Page: under the rug sign up form</span>

<span style="background: #A2E4A2;">When the app loads up the post template, we need to hide the global site footer (`.ws-footer`) by adding `.-hide`. This is because I'm fixing the register form to the bottom of the viewport so it appears under the last image. When a user has scrolled to the bottom of the page, we need to show the footer and add the register form to the flow of the document. To do this we:</span>

* <span style="background: #A2E4A2;">Add `.-flow` to `.ws-post-register, and</span>
* <span style="background: #A2E4A2;">Remove `.-hide` from `.ws-footer`</span>

##### <span style="background: #A2E4A2;">SPA page transitions</span>

<span style="background: #A2E4A2;">I've done something really basic for now, but it's enough to get us started: When the app is changing pages, add `.-rollout` to `.ws-header`. When the new view has loaded, remove the `.-rollout` class.</span>

##### <span style="background: #A2E4A2;">Tagline Rotator</span>

<span style="background: #A2E4A2;">The tagline rotator needs JS to make the rotator panel spin, and to change the content on the sides of the panel. I've commented the JS lines `88-162` in `main.js`.</span>

### <span style="background: #A2E4A2;">Edition Page</span>

<span style="background: #A2E4A2;">To show the edition page, we just need to prepend the HTML found in `.edition-header.html` to `main.ws-mag-home`</span>

##### <span style="background: #A2E4A2;">Edition headers dynamic behaviour:</span>

* <span style="background: #A2E4A2;">`.ws-edition`: add `background` by a `style` attribute, the value should be the edition color</span>
* <span style="background: #A2E4A2;">`.ws-edition-no-roman`: Add the edition number as a roman numeral</span>
* <span style="background: #A2E4A2;">`.ws-edition-title-name`: Add the edition name as text</span>
* <span style="background: #A2E4A2;">`.ws-edition-forward`: Prepend the edition forward</span>

# To swap between pages

* In `index.html` on line 34, swap out the text 'archive.html' for 'post.html' and you'll see the post page